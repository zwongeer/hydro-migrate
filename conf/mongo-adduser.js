// use admin;
// db.dropUser("hydro");
db.createUser({
    user: 'hydro',
    pwd: '${MONGODB_PASSWD}',
    roles: [{ role: 'readWrite', db: 'dbname' }]
});