#!/bin/bash
MINIO_ACCESS_KEY=`cat /dev/urandom | tr -cd a-zA-Z0-9 | head -c 32`
MINIO_SECRET_KEY=`cat /dev/urandom | tr -cd a-zA-Z0-9 | head -c 32`
MONGODB_PASSWD=`cat /dev/urandom | tr -cd a-zA-Z0-9 | head -c 32`