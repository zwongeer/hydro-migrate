#!/bin/bash
sshpass -p $REMOTE_PASSWD ssh root@$REMOTE_HOST "cd /root; systemctl stop nginx; mongodump -d hydro"
sshpass -p $REMOTE_PASSWD rsync -P -r root@$REMOTE_HOST:/root/dump .
mongorestore --drop dump