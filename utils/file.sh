#!/bin/bash
getfile() {
    if [ -z "$1" ]; then
        exit 1
    else
        eval "cat << EOF
$(< $1)
EOF"
    fi
}