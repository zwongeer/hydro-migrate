#!/bin/bash
if [ $USE_SS_PROXY = true ]; then
    if [ ! -f "/usr/bin/ss-local" ]; then
        apt install -y shadowsocks-libev
        systemctl disable --now shadowsocks-libev
    fi
    ss-local -s $SS_DOMAIN -p $SS_PORT -k $SS_PASSWD -m $SS_METHOD -l $SS_LOCAL_PORT > /dev/null 2>&1 &
    export ALL_PROXY=socks5://localhost:$SS_LOCAL_PORT
fi