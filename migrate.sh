#!/bin/bash
SHELL_FOLDER=$(dirname $(readlink -f "$0"))
cd $SHELL_FOLDER
error_trap () {
    local lc="$BASH_COMMAND" rc=$?
    echo "Failed:Command [$lc] exited with code [$rc]"
}
exit_trap() {
    if [ $USE_SS_PROXY = true ]; then
        kill $(lsof -t -i:$SS_LOCAL_PORT) > /dev/null 2>&1
    fi
    kill $(lsof -t -i:$REMOTE_MINIO_PORT) > /dev/null 2>&1
    kill $(jobs -p) > /dev/null 2>&1
    kill -9 $(jobs -p) > /dev/null 2>&1
}
trap error_trap ERR
trap exit_trap EXIT
set -e

source ./config.sh
source utils/file.sh

apt update && apt dist-upgrade -y
apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
    
utils/judge-tools.sh
apt install -y unzip snapd sshpass lsof gnupg aria2 rsync neofetch nginx-extras git vim wget nano htop tree net-tools telnet sudo ufw iftop iotop
apt purge -y certbot
sudo systemctl enable --now snapd

source utils/proxy_switch.sh

sudo snap install core; sudo snap refresh core; sudo snap install --classic certbot; 
if [ ! -f "/usr/bin/certbot" ]; then
    sudo ln -s /snap/bin/certbot /usr/bin/certbot
fi
ufw allow 22; ufw allow 80; ufw allow 443; yes|ufw enable
cp -r conf/nginx/* /etc/nginx/conf.d/
cp -r conf/renewal-hooks /etc/letsencrypt/
apt purge -y rclone
if [ ! -x "$(command -v rclone)" ]; then
    curl https://rclone.org/install.sh | sudo bash
fi

source utils/os_detect.sh
if [ ! -x "$(command -v mongo)" ]; then
    if [ "$DISTRO" == "ubuntu" ]; then
        wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
        echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
        apt update && apt install -y mongodb-org
    elif [ "$OS" == "debian" ]; then
        wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
        echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
        apt update && apt install -y mongodb-org
    elif [ "$OS" == "arch" ]; then
        echo "Not implemented yet"
        exit 1
    fi
fi
if [ ! -x "$(command -v minio)" ]; then
    wget https://dl.min.io/server/minio/release/linux-amd64/minio_20220314182524.0.0_amd64.deb
    dpkg -i minio_20220314182524.0.0_amd64.deb
    rm -rf minio_20220314182524.0.0_amd64.deb
fi
if [ ! -f "/usr/local/bin/hydro-sandbox" ]; then
    wget https://github.com/criyle/go-judge/releases/download/v1.4.2/executorserver-amd64 -O /usr/local/bin/hydro-sandbox
    chmod +x /usr/local/bin/hydro-sandbox
fi
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

nvm install --lts
nvm use --lts

source utils/gen_passwd.sh

getfile ./services/hydrooj.service > /lib/systemd/system/hydrooj.service
getfile ./services/hydro-sandbox.service > /lib/systemd/system/hydro-sandbox.service
cp -r conf/.hydro ~
getfile conf/.hydro/config.json > ~/.hydro/config.json
getfile conf/.hydro/env > ~/.hydro/env
getfile conf/minio > /etc/default/minio

systemctl daemon-reload
systemctl enable mongod
systemctl restart mongod
mkdir -p /data/minio
if ! id minio-user &>/dev/null ; then
    useradd --system minio-user --shell /sbin/nologin
    passwd -l minio-user
fi
chown -R minio-user:minio-user /data/minio
systemctl enable minio
systemctl restart minio
systemctl enable --now hydro-sandbox


getfile conf/mongo-adduser.js|iconv -t UTF-8 > ./mongo-adduser.js
mongo 127.0.0.1:27017/hydro ./mongo-adduser.js
rm ./mongo-adduser.js

unset ALL_PROXY
sshpass -p $REMOTE_PASSWD ssh -fN -L $REMOTE_MINIO_PORT:localhost:9000 root@$REMOTE_HOST

mkdir -p ~/.config/rclone
getfile conf/rclone.conf > ~/.config/rclone/rclone.conf

npm i -g yarn
# sshpass -p $REMOTE_PASSWD rsync -P -r --exclude "node_modules" --exclude "cache" root@$REMOTE_HOST:/root/hitwhoj ~
# cd /root/hitwhoj
# yarn install
# yarn build && yarn build:ui:production
cd /root/
git clone https://github.com/hitwhoj/hitwhoj-legacy.git hitwhoj
cd /root/hitwhoj
yarn install && yarn build && yarn build:ui:production
cd $SHELL_FOLDER

utils/minio_sync.sh # hot sync

utils/mongodb_sync.sh # stop the remote hydrooj server

echo "Done!"
eval "cat << EOF
MINIO_ACCESS_KEY=$MINIO_ACCESS_KEY
MINIO_SECRET_KEY=$MINIO_SECRET_KEY
MONGODB_PASSWD=$MONGODB_PASSWD
You could run:
systemctl enable --now hydrooj.service
certbot --nginx
systemctl enable --now nginx.service
EOF
"
